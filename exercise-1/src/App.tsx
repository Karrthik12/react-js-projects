import * as React from 'react';
import './App.css';
import { Grid, Row, Col, Alert, Button } from 'react-bootstrap';
import { createStore } from 'redux';

const dataSet = (state, action) => {
  switch (action.type) {
    case 'SUBMIT':
      return {
        name: action.name,
        email: action.email,
        password: action.password,
        errorMessage: action.errorMessage,
        error: action.error
      };
    default:
      return state;
  }
};

const dataSets = (state = [], action) => {
  switch (action.type) {
    case 'SUBMIT':
      return [
        ...state,
        dataSet(undefined, action)
      ];
    default:
      return state;
  }
};

const store = createStore<any>(dataSets);

const Counter = ({ value,
  submit }) => (
    <h1>data is submitted</h1>
  );

export default class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      name: this.props.defaultName,
      email: this.props.defaultMail,
      password: this.props.defaultPassword,
      confirmPassword: this.props.defaultRePassword,
      error_name: this.props.defaultErrorName,
      error_email: this.props.defaultErrorMail,
      error_password: this.props.defaultErrorPassword,
      error_confirmPassword: this.props.defaultErrorRePassword,
      show: false
    };
  }

  public handleDismiss(): void {
    this.setState({ show: false });
  }

  public onClick(): void {
    var emailPattern: any = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    if (this.state.error_email === '' && this.state.error_name === '' &&
      this.state.error_password === '' && this.state.error_confirmPassword === ''
      && this.state.password === this.state.confirmPassword) {
      store.dispatch({
        type: 'SUBMIT',
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        errorMessage: 'No Error',
        error: false
      });
      console.log(store.getState());
    } else if (this.state.error_email !== '' || !emailPattern.test(this.state.error_email)) {
      store.dispatch({
        type: 'SUBMIT',
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        errorMessage: 'Error in email',
        error: true
      });
      console.log(store.getState());
    } else if (this.state.error_name !== '') {
      store.dispatch({
        type: 'SUBMIT',
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        errorMessage: 'Error in name`',
        error: true
      });
      console.log(store.getState());
    } else if (this.state.error_password !== '') {
      store.dispatch({
        type: 'SUBMIT',
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        errorMessage: 'Error in Password`',
        error: true
      });
      console.log(store.getState());
    } else if (this.state.error_confirmPassword !== '' || this.state.password === this.state.confirmPassword) {
      store.dispatch({
        type: 'SUBMIT',
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        errorMessage: 'Error in Confirm password`',
        error: true
      });
      console.log(store.getState());
    }
  }

  public handleOnChange(event: any, type: string): void {
    switch (type) {
      case 'email':
        if (event.target.value >= 50) {
          this.setState({
            email: event.target.value.substring(0, 50),
          });
        }
        break;
      default:
        break;
    }
  }

  public handleOnBlur(event: any, type: string): void {
    switch (type) {
      case 'email':
        if (event.target.value.length >= 50) {
          this.setState({
            email: event.target.value,
            error_email: 'Maximum length is 50'
          });
          return;
        }
        var emailPattern: any = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        if (!emailPattern.test(event.target.value)) {
          this.setState({
            email: event.target.value,
            error_email: 'Please enter a valid email'
          });
        } else {
          this.setState({
            email: event.target.value,
            error_email: ''
          });
        }
        break;
      case 'name':
        if (event.target.value.length < 8 || event.target.value.length > 15) {
          this.setState({
            name: event.target.value,
            error_name: 'Username should be 8 to 15 charecters long'
          });
        } else {
          this.setState({
            name: event.target.value,
            error_name: ''
          });
        }
        break;
      case 'password':
        if (event.target.value.length < 8 || event.target.value.length > 15) {
          this.setState({
            password: event.target.value,
            error_password: 'Password should be atleast 8 characters long'
          });
        } else {
          this.setState({
            password: event.target.value,
            error_password: ''
          });
        }
        break;
      case 'repassword':
        if (!(event.target.value === this.state.password)) {
          this.setState({
            confirmPassword: event.target.value,
            error_confirmPassword: 'Password do not match'
          });
        } else {
          this.setState({
            confirmPassword: event.target.value,
            error_confirmPassword: ''
          });
        }
        break;
      default:
        break;
    }
  }

  public render() {
    let show: any;
    if (this.state.show) {
      show = 'show';
    } else {
      show = 'hide';
    }
    return (
      <div>
        <Grid>
          <Row className="show-grid margin-top">
            <Col className="right" md={2}>
              <label className="label-body">
                Email
              </label>
            </Col>
            <Col className="left" md={10}>
              <input type="email" onChange={e => this.handleOnChange(e, 'email')} onBlur={e => this.handleOnBlur(e, 'email')} />
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="left" mdOffset={2} md={10}>
              <p className="error">{this.state.error_email}</p>
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="right" md={2}>
              <label className="label-body">
                User Name
              </label>
            </Col>
            <Col className="left" md={10}>
              <input type="text"  onChange={e => this.handleOnChange(e, 'name')} onBlur={e => this.handleOnBlur(e, 'name')} />
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="left" mdOffset={2} md={10}>
              <p className="error">{this.state.error_name}</p>
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="right" md={2}>
              <label className="label-body">
                Password
              </label>
            </Col>
            <Col className="left" md={10}>
              <input type="password" onChange={e => this.handleOnChange(e, 'password')} onBlur={e => this.handleOnBlur(e, 'password')} />
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="left" mdOffset={2} md={10}>
              <p className="error">{this.state.error_password}</p>
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="right" md={2}>
              <label className="label-body">
                Confirm Password
              </label>
            </Col>
            <Col className="left" md={10}>
              <input type="password" onChange={e => this.handleOnChange(e, 'repassword')} onBlur={e => this.handleOnBlur(e, 'repassword')} />
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="left" mdOffset={2} md={10}>
              <p className="error">{this.state.error_confirmPassword}</p>
            </Col>
          </Row>
          <Row className="show-grid">
            <Col className="left" mdOffset={2} md={10}>
              <button onClick={e => this.onClick()}>
                Submit
              </button>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}
