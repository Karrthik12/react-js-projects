import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const scales = {
  r: 'Rupees',
  d: 'Dollars'
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = { currency: '', scale: 'r' };
    this.handleRupeesChange = this.handleRupeesChange.bind(this);
    this.handleDollarChange = this.handleDollarChange.bind(this);
  }

  handleDollarChange(currency) {
    this.setState({ scale: 'd', currency });
  }

  handleRupeesChange(currency) {
    this.setState({ scale: 'r', currency });
  }

  render() {
    const scale = this.state.scale;
    const currency = this.state.currency;
    const rupees = scale === 'd' ? tryConvert(currency, toRupees) : currency;
    const dollars = scale === 'r' ? tryConvert(currency, toDollars) : currency;

    return (
      <div>
        <Money scale="r" currency={rupees} onCurrencyChange={this.handleRupeesChange} />
        <Money scale="d" currency={dollars} onCurrencyChange={this.handleDollarChange} />
      </div>
    );
  }
}

function tryConvert(currency, convert) {
  const input = parseFloat(currency);
  if (Number.isNaN(input)) {
    return '';
  }
  const output = convert(input);
  const rounded = Math.round(output * 1000) / 1000;
  return rounded.toString();
}

function toRupees(dollars) {
  return dollars * 64.19;
}

function toDollars(rupees) {
  return rupees / 64.19;
}


class Money extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.onCurrencyChange(e.target.value);
  }

  render() {
    const currency = this.props.currency;
    const scale = this.props.scale;

    return (
      <fieldset>
        <legend>Enter currency in {scales[scale]}:</legend>
        <input value={currency}
          onChange={this.handleChange} />
      </fieldset>
    );

  }

}

export default App;
